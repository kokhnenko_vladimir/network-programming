﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace MulticastApp
{
    class Program
    {
        static IPAddress remoteAddress;
        const int remotePort = 8001;

        static void Main()
        {
            try
            {
                remoteAddress = IPAddress.Parse("235.5.5.11");

                Console.WriteLine("Сервер запущен");

                SendMessage();

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        private static void SendMessage()
        {
            UdpClient sender = new UdpClient();
            IPEndPoint endPoint = new IPEndPoint(remoteAddress, remotePort);

            try
            {
                while (true)
                {
                    byte[] message = Encoding.Unicode.GetBytes(DateTime.Now.ToString());
                    sender.Send(message, message.Length, endPoint);
                    Thread.Sleep(1000);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                sender.Close();
            }
        }
    }
}