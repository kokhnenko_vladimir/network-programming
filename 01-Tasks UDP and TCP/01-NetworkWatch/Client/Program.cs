﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MulticastApp
{
    class Program
    {
        static readonly UdpClient client = new UdpClient(8001);

        public static void Main()
        {
            WorkingAsync().Wait();
        }

        static async Task WorkingAsync()
        {
            try
            {
                client.JoinMulticastGroup(IPAddress.Parse("235.5.5.11"));

                await Task.Run(() =>
                {
                    while (true)
                    {
                        if (Console.KeyAvailable && ConsoleKey.Escape == Console.ReadKey(true).Key)
                            break;

                        Console.SetCursorPosition(0, 0);
                        IPEndPoint ip = null;
                        Console.WriteLine(Encoding.UTF8.GetString(client.Receive(ref ip)));
                    }
                });
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                client.Close();
            }
        }
    }
}